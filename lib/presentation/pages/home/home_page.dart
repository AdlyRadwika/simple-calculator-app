import 'package:calculator_app/presentation/pages/home/widgets/custom_button_gridview.dart';
import 'package:calculator_app/presentation/pages/home/widgets/input_and_value_widget.dart';
import 'package:calculator_app/presentation/provider/calculator_notifier.dart';
import 'package:flutter/material.dart';
import 'package:calculator_app/injection.dart' as di;
import 'package:provider/provider.dart';

class HomePage extends StatefulWidget {
  const HomePage({super.key});

  @override
  State<HomePage> createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  late CalculatorNotifier calculatorNotifier;

  @override
  void initState() {
    super.initState();

    calculatorNotifier = di.locator<CalculatorNotifier>();
    calculatorNotifier.initButtons();
  }

  @override
  Widget build(BuildContext context) {
    final screenHeight = MediaQuery.sizeOf(context).height;
    final theme = Theme.of(context);
    return ChangeNotifierProvider<CalculatorNotifier>(
        create: (_) => calculatorNotifier,
        builder: (context, _) {
          return Scaffold(
            appBar: AppBar(
              title: const Text("Calculator App"),
            ),
            backgroundColor: theme.colorScheme.secondary,
            body: Consumer<CalculatorNotifier>(builder: (context, provider, _) {
              return Column(
                children: [
                  SizedBox(
                    height: screenHeight * 0.3,
                    child: InputAndValueWidget(provider: provider)
                  ),
                  Expanded(child: ButtonGridview(provider: provider)),
                ],
              );
            }),
          );
        });
  }
}
