import 'package:flutter/material.dart';

class CustomButton extends StatefulWidget {
  final Color color;
  final Color textColor;
  final String buttonText;
  final Function onTap;

  const CustomButton(
      {super.key,
      required this.color,
      required this.textColor,
      required this.buttonText,
      required this.onTap});

  @override
  State<CustomButton> createState() => _CustomButtonState();
}

class _CustomButtonState extends State<CustomButton> with SingleTickerProviderStateMixin{
  late AnimationController _controller;

  @override
  void initState() {
    super.initState();

    _controller = AnimationController(
      duration: const Duration(milliseconds: 200),
      vsync: this
    );
  }

  @override
  void dispose() {
    super.dispose();

    _controller.dispose();
  }
  
  @override
  Widget build(BuildContext context) {
    final theme = Theme.of(context);
    return Container(
      decoration: BoxDecoration(
        shape: BoxShape.circle,
        color: widget.color,
      ),
      child: Material(
        type: MaterialType.transparency,
        child: InkWell(
          borderRadius: BorderRadius.circular(50.0),
          onTap: () {
            _controller.forward();
            Future.delayed(const Duration(milliseconds: 200), () => _controller.reverse(),);
            widget.onTap();
          },
          child: Center(
            child: ScaleTransition(
              scale: Tween<double>(
                begin: 1.0,
                end: 0.7,
              ).animate(_controller),
              child: Text(
                widget.buttonText,
                style: theme.textTheme.headlineMedium?.copyWith(
                  color: widget.textColor,
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }
}
