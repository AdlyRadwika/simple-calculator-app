import 'package:calculator_app/presentation/provider/calculator_notifier.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class InputAndValueWidget extends StatelessWidget {

  final CalculatorNotifier provider;
  
  const InputAndValueWidget({
    super.key, required this.provider,
  });

  @override
  Widget build(BuildContext context) {
    final theme = Theme.of(context);
    return ChangeNotifierProvider<ChangeNotifier>.value(
      value: provider,
      child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: [
            Container(
              padding: const EdgeInsets.all(20),
              alignment: Alignment.centerRight,
              child: SingleChildScrollView(
                controller: provider.userInputScroll,
                scrollDirection: Axis.horizontal,
                child: Text(
                  maxLines: 1,
                  provider.userInput,
                  style: theme.textTheme.headlineMedium?.copyWith(
                    color: theme.colorScheme.onPrimary,
                  ),
                ),
              ),
            ),
            const SizedBox(),
            Container(
              padding: const EdgeInsets.all(15),
              alignment: Alignment.centerRight,
              child: SingleChildScrollView(
                scrollDirection: Axis.horizontal,
                controller: provider.valueScroll,
                child: Text(
                  provider.value,
                  maxLines: 1,
                  overflow: TextOverflow.ellipsis,
                  style: theme.textTheme.displaySmall?.copyWith(
                    color: theme.colorScheme.onPrimary,
                    fontWeight: FontWeight.bold,
                  ),
                ),
              ),
            )
          ]),
    );
  }
}