import 'package:calculator_app/presentation/pages/home/widgets/custom_button.dart';
import 'package:calculator_app/presentation/provider/calculator_notifier.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class ButtonGridview extends StatelessWidget {
  final CalculatorNotifier provider;

  const ButtonGridview({
    super.key,
    required this.provider,
  });

  @override
  Widget build(BuildContext context) {
    final theme = Theme.of(context);
    return ChangeNotifierProvider<CalculatorNotifier>.value(
      value: provider,
      child: GridView.builder(
        padding: const EdgeInsets.all(10.0),
        physics: const NeverScrollableScrollPhysics(),
          itemCount: provider.buttons.length,
          gridDelegate: const SliverGridDelegateWithFixedCrossAxisCount(
            crossAxisCount: 4,
            crossAxisSpacing: 15.0,
            mainAxisSpacing: 10.0,
          ),
          itemBuilder: (BuildContext context, int index) {
            if (index == 0) {
              return CustomButton(
                onTap: () => provider.clear(),
                buttonText: provider.buttons[index],
                color: theme.colorScheme.error,
                textColor: Colors.white,
              );
            } else if (index == 1) {
              return CustomButton(
                buttonText: provider.buttons[index],
                onTap: () => provider.determinePlusMinus(),
                color: theme.colorScheme.primary,
                textColor: Colors.white,
              );
            } else if (index == 2) {
              return CustomButton(
                onTap: () => provider.commonInput(index),
                buttonText: provider.buttons[index],
                color: theme.colorScheme.primary,
                textColor: Colors.white,
              );
            } else if (index == 3) {
              return CustomButton(
                onTap: () => provider.delete(),
                buttonText: provider.buttons[index],
                color: theme.colorScheme.error,
                textColor: Colors.white,
              );
            } else if (index == 18) {
              return CustomButton(
                onTap: () => provider.calculate(),
                buttonText: provider.buttons[index],
                color: theme.colorScheme.surface,
                textColor: Colors.black,
              );
            } else {
              return CustomButton(
                onTap: () => provider.commonInput(index),
                buttonText: provider.buttons[index],
                color: provider.isOperator(provider.buttons[index])
                    ? theme.colorScheme.primary
                    : Colors.white,
                textColor: provider.isOperator(provider.buttons[index])
                    ? Colors.white
                    : Colors.black,
              );
            }
          }),
    );
  }
}
