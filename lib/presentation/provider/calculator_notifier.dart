import 'package:flutter/material.dart';
import 'package:math_expressions/math_expressions.dart';

class CalculatorNotifier extends ChangeNotifier {
  String _userInput = "";
  String get userInput => _userInput;

  final ScrollController _userInputScroll = ScrollController();
  ScrollController get userInputScroll => _userInputScroll;

  String _value = "";
  String get value => _value;

  final ScrollController _valueScroll = ScrollController();
  ScrollController get valueScroll => _valueScroll;

  late List<String> buttons;

  void initButtons() {
    buttons = [
      'C',
      '+/-',
      '%',
      'DEL',
      '7',
      '8',
      '9',
      '/',
      '4',
      '5',
      '6',
      'x',
      '1',
      '2',
      '3',
      '-',
      '0',
      '.',
      '=',
      '+',
    ];
    notifyListeners();
  }

  bool isOperator(String x) {
    if (x == '/' || x == 'x' || x == '-' || x == '+' || x == '=') {
      return true;
    }
    return false;
  }

  void onUserInputEvent() {
    _userInputScroll.animateTo(
      _userInputScroll.position.maxScrollExtent,
      duration: const Duration(milliseconds: 200),
      curve: Curves.easeIn
    );
    notifyListeners();
  }

  void resetOnClearEvent() {
    _userInputScroll.jumpTo(_userInputScroll.position.minScrollExtent);
    _valueScroll.jumpTo(_valueScroll.position.minScrollExtent);
    notifyListeners();
  }

  void onCalculateEvent() {
    _valueScroll.animateTo(
      _valueScroll.position.maxScrollExtent + 100,
      duration: const Duration(milliseconds: 500),
      curve: Curves.easeIn
    );
    notifyListeners();
  }

  void clear() {
    _userInput = "";
    _value = "0";
    resetOnClearEvent();
    notifyListeners();
  }

  void commonInput(int i) {
    _userInput += buttons[i];
    onUserInputEvent();
    notifyListeners();
  }

  void determinePlusMinus() {
    if (!userInput.contains("-")) {
      _userInput = _userInput.replaceFirst('', '-');
    } else {
      _userInput = _userInput.replaceFirst('-', '');
    }
    onUserInputEvent();
    notifyListeners();
  }

  void delete() {
    _userInput = _userInput.substring(0, _userInput.length - 1);
    onUserInputEvent();
    notifyListeners();
  }

  void calculate() {
    try {
      String finaluserinput = userInput;
      finaluserinput = userInput.replaceAll('x', '*');

      Parser p = Parser();
      Expression exp = p.parse(finaluserinput);
      ContextModel cm = ContextModel();
      double eval = exp.evaluate(EvaluationType.REAL, cm);
      _value = eval.toString();
      onCalculateEvent();
      notifyListeners();
    } catch (e) {
      debugPrint(e.toString());
      _value = "0";
      notifyListeners();
    }
  }
}
