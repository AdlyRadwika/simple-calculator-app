import 'package:calculator_app/presentation/provider/calculator_notifier.dart';
import 'package:get_it/get_it.dart';

final locator = GetIt.instance;

void init() {
  //Provider
  locator.registerFactory<CalculatorNotifier>(() => CalculatorNotifier());
}
