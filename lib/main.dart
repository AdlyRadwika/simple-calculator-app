import 'package:calculator_app/common/theme.dart';
import 'package:calculator_app/presentation/route.dart' as route;
import 'package:flutter/material.dart';
import 'package:calculator_app/injection.dart' as di;

void main() {
  WidgetsFlutterBinding.ensureInitialized();
  di.init();
  runApp(const MainApp());
}

class MainApp extends StatelessWidget {
  const MainApp({super.key});

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Calculator App',
      theme: myThemeData,
      onGenerateRoute: route.controller,
      initialRoute: route.homePage,
    );
  }
}
